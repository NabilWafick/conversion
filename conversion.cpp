#include <iostream>
#include "fonctions.h"
#include "conversion.h"
#include <string>
#include <ctype.h>

using namespace std;

/// fonction conversionDecimalBinaire
string conversionDecimalBinaire(string& valeur)
{
    string valeurRetour(" ");

    if(appartiennentABaseElementsValeur(valeur, 10))
    {
        unsigned int tailleValeur(valeur.size()), nombreEquival(0);
        unsigned int* tab(0);
        tab=new unsigned int [tailleValeur];  /// allocation d'un espace m�moire (d'un tableau) devant contenir les valeurs entiers correspondant aux �l�ments  de vsaleur

        if(tab != 0)
        {
            valeurElementsDansTab(valeur, tab, 10);   /// insertion des nombres entiers correspondant � chaque caract�re dans tab (valeur >> "10109" => tab >> 10109 )
            nombreEquival=nombreFromBase(tab, tailleValeur, 10);  ///calcul du nombre d�cimal correspondant � la valeur entr�e par la m�thode de l'�levation � la puissance
            delete tab; /// la m�moire allou�e est supprim�e/lib�r�e
            tab=0;

            unsigned int numberRestes(nombreRestesDivisionEuclidienne(nombreEquival, 2));
            tab=new unsigned int [numberRestes];  /// r�allocation de la m�moire lib�r�e mais de taille diff�rente qui servira � stocker les restes issus de la division euclidienne
                                                                                          /// de nombreEquival par la base
        /// nombreEquival �tant le nombre issus de la conversion en d�cimal du nombre binaire entr�
        /// �tant donn� que les donn�es sont g�r�es sous forme de string, le nombre d�cimal obtenu devra �tre converti en string.Ainsi, on fais la division euclidienne du nombre obtenu
        /// en base dix en stockant les restes dans tab, ce qui permettra de chercher les caract�res correspondants� chaque reste

            if(tab != 0)
            {
                divisionEuclidienne(nombreEquival, 2, tab, numberRestes);  /// division euclidienne de nombreEquival correspondant � valeur
                charactereNombreDansValeurRetour(tab, numberRestes, valeurRetour, 2);  /// identification des caract�res correspondant � chaque reste
                valeurRetour.erase(0, 1);
            }
            else
            {
                cout<<"ERREUR X :: Probleme de Memoire ! "<<endl;
            }
            delete tab;   /// lib�ration de la m�moire allou�e
            tab=0;
        }
        else
        {
            cout<<"ERREUR X :: Probleme de Memoire !"<<endl;
        }
        delete tab;
        tab=0;
    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 10 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 10 : ";
        cin>>valeur;
        valeurRetour=conversionDecimalBinaire(valeur);
    }

    return valeurRetour;
}

/// fonction conversionDecimalOctal
string conversionDecimalOctal(string& valeur)
{
    string valeurRetour(" ");

    if(appartiennentABaseElementsValeur(valeur, 10))
    {
        unsigned int tailleValeur(valeur.size()), nombreEquival(0);
        unsigned int* tab(0);
        tab=new unsigned int [tailleValeur];

        if(tab != 0)
        {
            valeurElementsDansTab(valeur, tab, 10);
            nombreEquival=nombreFromBase(tab, tailleValeur, 10);
            delete tab;
            tab=0;

            unsigned int numberRestes(nombreRestesDivisionEuclidienne(nombreEquival, 8));
            tab=new unsigned int [numberRestes];

            if(tab != 0)
            {
                divisionEuclidienne(nombreEquival, 8, tab, numberRestes);
                charactereNombreDansValeurRetour(tab, numberRestes, valeurRetour, 8);
                valeurRetour.erase(0, 1);
            }
            else
            {
                cout<<"ERREUR X :: Probleme de Memoire ! "<<endl;
            }
            delete tab;
            tab=0;
        }
        else
        {
            cout<<"ERREUR X :: Probleme de Memoire !"<<endl;
        }
        delete tab;
        tab=0;
    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 10 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 10 : ";
        cin>>valeur;
        valeurRetour=conversionDecimalOctal(valeur);
    }

    return valeurRetour;
}

/// fonction conversionDecimalHexadecimal
string conversionDecimalHexadecimal(string& valeur)
{
    string valeurRetour(" ");

    if(appartiennentABaseElementsValeur(valeur, 10))
    {
        unsigned int tailleValeur(valeur.size()), nombreEquival(0);
        unsigned int* tab(0);
        tab=new unsigned int [tailleValeur];

        if(tab != 0)
        {
            valeurElementsDansTab(valeur, tab, 10);
            nombreEquival=nombreFromBase(tab, tailleValeur, 10);
            delete tab;
            tab=0;

            unsigned int numberRestes(nombreRestesDivisionEuclidienne(nombreEquival, 16));
            tab=new unsigned int [numberRestes];

            if(tab != 0)
            {
                divisionEuclidienne(nombreEquival, 16, tab, numberRestes);
                charactereNombreDansValeurRetour(tab, numberRestes, valeurRetour, 16);
                valeurRetour.erase(0, 1);
            }
            else
            {
                cout<<"ERREUR X :: Probleme de Memoire ! "<<endl;
            }
            delete tab;
            tab=0;
        }
        else
        {
            cout<<"ERREUR X :: Probleme de Memoire !"<<endl;
        }
        delete tab;
        tab=0;
    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 10 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 10 : ";
        cin>>valeur;
        valeurRetour=conversionDecimalHexadecimal(valeur);
    }

    return valeurRetour;
}

/// fonction conversionBinaireOctal
string conversionBinaireOctal(string& valeur)
{
    string valeurRetour(" "), troisBits("000");
/// troisBits >> chaine de caract�res qui permettra de convertir en regroupant par trois bits
/// Exemple : 101100011 => 101 100 011 => 5 4 3 || 1000010 => 1 000 010 => 001 000 010 => 1 0 2
/// troisBits permettra de stocker les bits par bon de trois en comptant et d�butant de la gauche

    if(appartiennentABaseElementsValeur(valeur, 2))
    {
        unsigned int* tab(0);
        tab=new unsigned int [3];  /// allocation d'espace m�moire(un tableau de trois entiers) qui permettra de stocker les nombres entiers correspondant � chaque groupe de trois bits
        /// troisBits >> "101" => tab >> 101

        if(tab != 0)
        {
            unsigned int i(0), tailleValeurRetour(valeurRetour.size()), tailleValeur(valeur.size()), nombreEquival(0);

            /// initialisation de la taille de valeurRetour, cha�ne de caract�res qui contiendra le r�sultat issus de la conversion (r�sultat final)

            if(tailleValeur % 3 == 0) /// si la taille de valeur est un multiple de trois alors valeurRetour est augment� de tailleValeur/3 -1 ( -1 d� au premier espace d�j� initialiser)
            /// valeur >> 100101010 <=> 100 101 010 => valeurRetour >> valeurRetour(" ") + ' ' +' '
            {
                for(i=0; i<((tailleValeur / 3) - 1); i++)
                {
                    valeurRetour.push_back(' ');
                }
            }
            else /// sinon valeurRetour est augnent� de tailleValeur/3
            /// valeur >> 1101001 <=> 1 101 001 <=> 001 101  001 => valeurRetour >> valeurRetour(" ") + ' ' + ' '
            {
                for(i=0; i<(tailleValeur / 3); i++)
                {
                    valeurRetour.push_back(' ');
                }

                tailleValeurRetour=valeurRetour.size(); /// determination et stockage de la taille de valeurRetour
                unsigned int nbreZerosAjoutes((tailleValeurRetour * 3) % tailleValeur);  /// determination du nombre de z�ros � ajoueter � valeur pour obtenir des triplets complets

                for(i=0; i<nbreZerosAjoutes; i++)
                {
                    valeur.push_back('0');
                }

                zerosRamenesAuDebut(valeur, nbreZerosAjoutes);  /// les z�ros sont ajout�s avec push_back et ainsi donc ajout�s en fin de valeur, il faudra les ramen�s au d�but pour ne pas
                /// d�former valeur.

            }

            unsigned int incre(0), j(0);
            tailleValeur=valeur.size();
            tailleValeurRetour=valeurRetour.size();

            for(i=0; i<tailleValeur; i++)
            {
                troisBits[j]=valeur[i];  /// insertion des bits par triplet dans troisBits
                if( j == 2)  /// si un triplet de bits est ins�r� dans troisBits
                {
                    valeurElementsDansTab(troisBits, tab, 2); /// les valeurs (entiers) correspondants � chaque bits du triplet sont ins�r�s dans tab pour �tre converti en d�cimal
                    nombreEquival=nombreFromBase(tab, 3, 2); /// d�termination du nombre �quivalent par l'�l�vation de puissance
                    valeurRetour[incre]=elementEquivalantNombre(nombreEquival, 8); /// le caract�re correspondant � nombreEquival est ins�r� dans une case de valeurRetour
                     incre++;  /// incrementation pour passer � la case suivante de valeurRetour
                    j=0;  /// remise � z�ro de la variable d'incr�mentation de troisBits afin de recommencer l'insertion d'un autre triplet
                }
                else
                    j++;  /// incr�mentation pour passer par chaque case de troisBits

            }
        }
        else
        {
            cout<<" ERREUR X :: Probleme de Memoire ! "<<endl;
        }
        delete tab;
        tab=0;

    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 2 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 2 : ";
        cin>>valeur;
        valeurRetour=conversionBinaireOctal(valeur);
    }
    return valeurRetour;
}

/// fonction  conversionBinaireDecimal
string conversionBinaireDecimal(string& valeur)
{
    string valeurRetour(" ");

    if(appartiennentABaseElementsValeur(valeur, 2))
    {
        unsigned int tailleValeur(valeur.size()), nombreEquival(0);
        unsigned int* tab(0);
        tab=new unsigned int [tailleValeur];  /// alloation de m�moire � la taille de valeur afin d'y stocker les nombres correspondant aux caract�res de valeur

        if(tab != 0)
        {
            valeurElementsDansTab(valeur, tab, 2);  /// insertion des nombres correspondants dans tab
            nombreEquival=nombreFromBase(tab, tailleValeur, 2);  /// calcul du nombre �quivalent (en d�cimal ) correspondant au  nombre en binaire
            delete tab;  /// lib�ration de la m�moire allou�e
            tab=0;

            unsigned int numberRestes(nombreRestesDivisionEuclidienne(nombreEquival, 10));  /// d�termination du nombre de restes de la division euclidienne de nombreEquival par la base
            tab=new unsigned int [numberRestes]; /// alllocation de la m�moire (tableau) qui contiendra les restes

            if(tab != 0)
            {
                divisionEuclidienne(nombreEquival, 10, tab, numberRestes);  /// division euclidienne de nombreEquival en base 10
                charactereNombreDansValeurRetour(tab, numberRestes, valeurRetour, 10);  /// insertion dans valeurRetour des caract�res correspondants aux restes
                valeurRetour.erase(0, 1);  /// suppression du premier caract�re de valeurRetour(' ') initialiser lors de la d�claration
            }
            else
            {
                cout<<"ERREUR X :: Probleme de Memoire ! "<<endl;
            }
            delete tab;
            tab=0;

        }
        else
        {
            cout<<" ERREUR X :: Probleme de Memoire ! "<<endl;
        }
        delete tab;
        tab=0;
    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 2 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 2 : ";

        cin>>valeur;
        valeurRetour=conversionBinaireDecimal(valeur);
    }
    return valeurRetour;
}

/// fonction conversionBinaireHexadecimal
string conversionBinaireHexadecimal(string& valeur)
{
    string valeurRetour(" "), quatreBits("0000");
/// quatreBits >> chaine de caract�res qui permettra de convertir en regroupant par quatre bits
/// Exemple : 101100011101 => 1011 0001 1101 => B 1 C || 100001001 => 1 0000 1001 => 0001 0000 1001 => 1 0 2
/// quatreBits permettra de stocker les bits par bon de quatre en comptant et d�butant de la gauche

    if(appartiennentABaseElementsValeur(valeur, 2))
    {
        unsigned int* tab(0);
        tab=new unsigned int [4];  /// allocation d'espace m�moire(un tableau de trois entiers) qui permettra de stocker les nombres entiers correspondant � chaque groupe de quatre bits
        /// quatreBits >> "1010" => tab >> 1010

        if(tab != 0)
        {
            unsigned int i(0), tailleValeurRetour(valeurRetour.size()), tailleValeur(valeur.size()), nombreEquival(0);

            /// initialisation de la taille de valeurRetour, cha�ne de caract�res qui contiendra le r�sultat issus de la conversion (r�sultat final)

            if(tailleValeur % 4 == 0) /// si la taille de valeur est un multiple de quatre alors valeurRetour est augment� de tailleValeur/4 -1 ( -1 d� au premier espace d�j� initialiser)
            /// valeur >> 100101010000 <=> 1001 0101 0000 => valeurRetour >> valeurRetour(" ") + ' ' +' '
            {
                for(i=0; i<((tailleValeur / 4) - 1); i++)
                {
                    valeurRetour.push_back(' ');
                }
            }
            else /// sinon valeurRetour est augnent� de tailleValeur/4
            /// valeur >> 110100100 <=> 1 1010 0100 <=> 0001 1010  0100 => valeurRetour >> valeurRetour(" ") + ' ' + ' '
            {
                for(i=0; i<(tailleValeur / 4); i++)
                {
                    valeurRetour.push_back(' ');
                }

                tailleValeurRetour=valeurRetour.size(); /// d�termination et stockage de la taille de valeurRetour
                unsigned int nbreZerosAjoutes((tailleValeurRetour * 4) % tailleValeur);  /// d�termination du nombre de z�ros � ajoueter � valeur pour obtenir des quadruplets complets

                for(i=0; i<nbreZerosAjoutes; i++)
                {
                    valeur.push_back('0');
                }

                zerosRamenesAuDebut(valeur, nbreZerosAjoutes);  /// les z�ros sont ajout�s avec push_back et ainsi donc ajout�s en fin de valeur, il faudra les ramen�s au d�but pour ne pas
                /// d�former valeur.
            }

            unsigned int incre(0), j(0);
            tailleValeur=valeur.size();
            tailleValeurRetour=valeurRetour.size();

            for(i=0; i<tailleValeur; i++)
            {
                quatreBits[j]=valeur[i];  /// insertion des bits par quadruplet dans quatreBits
                if( j == 3)  /// si un quadruplet de bits est ins�r� dans quatreBits
                {
                    valeurElementsDansTab(quatreBits, tab, 2); /// les valeurs (entiers) correspondants � chaque bits du quadruplet sont ins�r�s dans tab pour �tre converti en d�cimal
                    nombreEquival=nombreFromBase(tab, 4, 2); /// d�termination du nombre �quivalent par l'�l�vation de puissance
                    valeurRetour[incre]=elementEquivalantNombre(nombreEquival, 16); /// le caract�re correspondant � nombreEquival est ins�r� dans une case de valeurRetour
                     incre++;  /// incrementation pour passer � la case suivante de valeurRetour
                    j=0;  /// remise � z�ro de la variable d'incr�mentation de troisBits afin de recommencer l'insertion d'un autre quadruplet
                }
                else
                    j++;  /// incr�mentation pour passer  par chaque case de quatreBits

            }
        }
        else
        {
            cout<<" ERREUR X :: Probleme de Memoire ! "<<endl;
        }
        delete tab;
        tab=0;

    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 2 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 2 : ";
        cin>>valeur;
        valeurRetour=conversionBinaireHexadecimal(valeur);
    }
    return valeurRetour;
}

/// fonction conversionOctalBinaire
string conversionOctalBinaire(string& valeur)
{
    string valeurRetour(" "), troisBits(" ");
/// troisBits >> cha�ne de caract�re qui contiendra les restes issus de la division euclidienne du nombre correpondant � chaque �l�ment de valeur en base 2
/// valeurRetour >> valeur � retourner
    if(appartiennentABaseElementsValeur(valeur, 8))
    {
        unsigned int tailleValeur(valeur.size());
        unsigned int* tab(0);
        tab=new unsigned int [tailleValeur];  /// allocation de m�moire devant servir � stocker les entiers correspondant aux �l�ments de valeurs

        if(tab != 0)
        {
            valeurElementsDansTab(valeur, tab, 8);  /// insertion dans tab des nombres correspondant aux caract�rs de valeur

            unsigned int tabRestes[3]= {0, 0, 0};  /// tab qui permettra � stocker les restes de la divison euclidienne de chaque nombre de tab en base 2

            for(unsigned int i(0); i<(3*tailleValeur - 1); i++)
            {
                valeurRetour.push_back(' ');    /// initialisation de la traille de valeuraretour qui fait 3*tailleValeur car chaque nombre de tab sera �crit sur trois bits
            }                                                               /// (-1 d� � l'initialisation initiale de valeurRetour(" "))

            int j(0), incre(0), bits(0);

            for(unsigned int i(0); i<tailleValeur; i++)
            {
                tabNull(tabRestes, 3);
                divisionEuclidienne(tab[i], 2, tabRestes, 3); /// �criture de chaque nombre de tab en binaire sur trois bits
                charactereNombreDansValeurRetour(tabRestes, 3, troisBits, 2);   /// �criture sous forme de cha�ne de caract�res du nombre binaire
                troisBits.erase(0,1);  /// suppression du premier �l�ment de troisBits
                bits=0;
                for(j=incre ; j<(3+incre); j++)   /// insertion des bits(issus de la division euclidienne et transform�s sous forme de cha�ne de caract�res) dans valeurRetour
                {
                    valeurRetour[j]=troisBits[bits];
                    bits++;
                }
                incre=j;

                troisBits=" ";  /// r�initialisation de troisBits d� � la fonction charactereNombreDansValeur dans laquelle est utilis� push_back sur troisBits
            }
        }
        else
        {
            cout<<" ERREUR X :: Probleme de Memoire ! "<<endl;
        }
        delete tab;
        tab=0;
    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 8 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 8 : ";
        cin>>valeur;
        valeurRetour=conversionOctalBinaire(valeur);
    }
    return valeurRetour;
}

/// fonction  conversionOctalDecimal
string conversionOctalDecimal(string& valeur)
{
    string valeurRetour(" ");

    if(appartiennentABaseElementsValeur(valeur, 8))
    {
        unsigned int tailleValeur(valeur.size()), nombreEquival(0);
        unsigned int* tab(0);
        tab=new unsigned int [tailleValeur];

        if(tab != 0)
        {
            valeurElementsDansTab(valeur, tab, 8);
            nombreEquival=nombreFromBase(tab, tailleValeur, 8);
            delete tab;
            tab=0;

            unsigned int numberRestes(nombreRestesDivisionEuclidienne(nombreEquival, 10));
            tab=new unsigned int [numberRestes];

            if(tab != 0)
            {
                divisionEuclidienne(nombreEquival, 10, tab, numberRestes);
                charactereNombreDansValeurRetour(tab, numberRestes, valeurRetour, 10);
                valeurRetour.erase(0, 1);
            }
            else
            {
                cout<<"ERREUR X :: Probleme de Memoire ! "<<endl;
            }
            delete tab;
            tab=0;

        }
        else
        {
            cout<<" ERREUR X :: Probleme de Memoire ! "<<endl;
        }
        delete tab;
        tab=0;
    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 8 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 8 : ";
        cin>>valeur;
        valeurRetour=conversionOctalDecimal(valeur);
    }
    return valeurRetour;
}

/// fonction  conversionOctalHexadecimal
string conversionOctalHexadecimal(string& valeur)
{
    string valeurRetour(" ");

    if(appartiennentABaseElementsValeur(valeur, 8))
    {
        valeurRetour=conversionOctalDecimal(valeur);
        valeurRetour=conversionDecimalHexadecimal(valeurRetour);
    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 8 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 8 : ";
        cin>>valeur;
        valeurRetour=conversionOctalHexadecimal(valeur);
    }
    return valeurRetour;
}

/// fonction conversionHexadecimalBinaire
string conversionHexadecimalBinaire(string& valeur)
{
    string valeurRetour(" "), quatreBits(" ");
/// quatreBits >> cha�ne de caract�re qui contiendra les restes issus de la division euclidienne du nombre correpondant � chaque �l�ment de valeur en base 2
/// valeurRetour >> valeur � retourner
    if(appartiennentABaseElementsValeur(valeur, 16))
    {
        unsigned int tailleValeur(valeur.size());
        unsigned int* tab(0);
        tab=new unsigned int [tailleValeur];  /// allocation de m�moire devant servir � stocker les entiers correspondant aux �l�ments de valeurs

        if(tab != 0)
        {
            valeurElementsDansTab(valeur, tab, 16);  /// insertion dans tab des nombres correspondant aux caract�rs de valeur

            unsigned int tabRestes[4]= {0, 0, 0, 0};  /// tab qui permettra � stocker les restes de la divison euclidienne de chaque nombre de tab en base 2

            for(unsigned int i(0); i<(4*tailleValeur - 1); i++)
            {
                valeurRetour.push_back(' ');    /// initialisation de la traille de valeuraretour qui fait 4*tailleValeur car chaque nombre de tab sera �crit sur quatre bits
            }                                                               /// (-1 d� � l'initialisation initiale de valeurRetour(" "))

            int j(0), incre(0), bits(0);

            for(unsigned int i(0); i<tailleValeur; i++)
            {
                 tabNull(tabRestes, 4);
                divisionEuclidienne(tab[i], 2, tabRestes, 4); /// �criture de chaque nombre de tab en binaire sur quatre bits
                charactereNombreDansValeurRetour(tabRestes, 4, quatreBits, 2);   /// �criture sous forme de cha�ne de caract�res du nombre binaire
                quatreBits.erase(0,1);  /// suppression du premier �l�ment de quatreBits


                bits=0;
                for(j=incre ; j<(4+incre); j++)   /// insertion des bits(issus de la division euclidienne et transform�s sous forme de cha�ne de caract�res) dans valeurRetour
                {

                    valeurRetour[j]=quatreBits[bits];
                    bits++;
                }
                incre=j;
                quatreBits=" ";  /// r�initialisation de quatreBits d� � la fonction charactereNombreDansValeur dans laquelle est utilis� push_back sur quatreBits
            }
        }
        else
        {
            cout<<" ERREUR X :: Probleme de Memoire ! "<<endl;
        }
        delete tab;
        tab=0;
    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 16 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 16 : ";
        cin>>valeur;
        valeurRetour=conversionHexadecimalBinaire(valeur);
    }
    return valeurRetour;
}

/// fonction conversionHexadecimalOctal
string conversionHexadecimalOctal(string& valeur)
{
    string valeurRetour(" ");

    if(appartiennentABaseElementsValeur(valeur, 16))
    {
        valeurRetour=conversionHexadecimalDecimal(valeur);
        valeurRetour=conversionDecimalOctal(valeurRetour);
    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 16 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 16 : ";
        cin>>valeur;
        valeurRetour=conversionHexadecimalOctal(valeur);
    }
    return valeurRetour;
}

/// fonction  conversionHexadecimalDecimal
string conversionHexadecimalDecimal(string& valeur)
{
    string valeurRetour(" ");

    if(appartiennentABaseElementsValeur(valeur, 16))
    {
        unsigned int tailleValeur(valeur.size()), nombreEquival(0);
        unsigned int* tab(0);
        tab=new unsigned int [tailleValeur];

        if(tab != 0)
        {
            valeurElementsDansTab(valeur, tab, 16);
            nombreEquival=nombreFromBase(tab, tailleValeur, 16);
            delete tab;
            tab=0;

            unsigned int numberRestes(nombreRestesDivisionEuclidienne(nombreEquival, 10));
            tab=new unsigned int [numberRestes];

            if(tab != 0)
            {
                divisionEuclidienne(nombreEquival, 10, tab, numberRestes);
                charactereNombreDansValeurRetour(tab, numberRestes, valeurRetour, 10);
                valeurRetour.erase(0, 1);
            }
            else
            {
                cout<<"ERREUR X :: Probleme de Memoire ! "<<endl;
            }
            delete tab;
            tab=0;

        }
        else
        {
            cout<<" ERREUR X :: Probleme de Memoire ! "<<endl;
        }
        delete tab;
        tab=0;
    }
    else
    {
        cout<<"La valeur entr�e n'est pas dans la base 16 !"<<endl;
        cout<<"Veuillez entrer une valeur appartenant a la base 16 : ";
        cin>>valeur;
        valeurRetour=conversionHexadecimalDecimal(valeur);
    }
    return valeurRetour;
}

/// fonction conversion
string conversion(string& valeur, unsigned int baseOrigine,  unsigned int baseConversion)
{
    string valeurRetour(" ");

    switch (baseOrigine)
    {
    case 2 :
    {
        switch (baseConversion)
        {
        case 8 :
        {
            valeurRetour=conversionBinaireOctal(valeur);
        }
        break;
        case 10 :
        {
            valeurRetour=conversionBinaireDecimal(valeur);
        }
        break;
        case 16 :
        {
            valeurRetour=conversionBinaireHexadecimal(valeur);
        }
        break;
        }
    }
    break;
    case 8  :
    {
        switch (baseConversion)
        {
        case 2 :
        {
            valeurRetour=conversionOctalBinaire(valeur);
        }
        break;
        case 10 :
        {
            valeurRetour=conversionOctalDecimal(valeur);
        }
        break;
        case 16 :
        {
            valeurRetour=conversionOctalHexadecimal(valeur);
        }
        break;
        }
    }
    break;
    case 10 :
    {
        switch (baseConversion)
        {
        case 2 :
        {
            valeurRetour=conversionDecimalBinaire(valeur);
        }
        break;
        case 8 :
        {
            valeurRetour=conversionDecimalOctal(valeur);
        }
        break;
        case 16 :
        {
            valeurRetour=conversionDecimalHexadecimal(valeur);
        }
        break;
        }
    }
    break;
    case 16 :
    {
        lettreCapitale(valeur);

        switch (baseConversion)
        {
        case 2 :
        {
            valeurRetour=conversionHexadecimalBinaire(valeur);
        }
        break;
        case 8 :
        {
            valeurRetour=conversionHexadecimalOctal(valeur);
        }
        break;
        case 10 :
        {
            valeurRetour=conversionHexadecimalDecimal(valeur);
        }
        break;
        }
    }
    break;
    }

    return valeurRetour;
}
