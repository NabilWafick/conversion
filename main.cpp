#include <iostream>
#include <string>
#include "fonctions.h"
#include "conversion.h"

using namespace std;

int main()
{

    string valeur(" "), valeurRetour(" ");
    unsigned int baseOrigine(1), baseConversion(1);
      char answer('O');

    cout<<endl<<" SALUT ! "<<endl
        <<"Bienvenue sur le Programme Conversion ! "<<endl
        <<"Bases de conversion :  2   8   10  16"<<endl;

    do
    {
        cout<<endl<<"Veuillez entrer la base du nombre a convertir : ";
        baseOrigine=cinBase();
        cout<<"Veuillez entrer la base dans laquelle sera convertie votre nombre : ";
        baseConversion=cinBase();

        cout<<"Veuillez entrer le nombre a convertir en base "<<baseConversion<<" : ";
        cin>>valeur;
        sansZerosDebut(valeur);

        lettreCapitale(valeur);

        while(appartiennentABaseElementsValeur(valeur, baseOrigine) == false)
        {
            cout<<"Le nombre entre n'appartient pas a la  base "<<baseOrigine<<" ! "<<endl
                <<"Veuillez entrer un nombre appartenant a la base "<<baseOrigine<<" s'il vous plait ! "<<endl
                <<"Votre nombre : ";
            cin>>valeur;

        }

        valeurRetour=conversion(valeur, baseOrigine, baseConversion);
        sansZerosDebut(valeur);
        sansZerosDebut(valeurRetour);

        cout<<endl<<"Conversion"<<endl;
        cout<<"Base "<<baseOrigine<<" : "<<valeur<<" <=> "<<"Base "<<baseConversion<<" : "<<valeurRetour ;

        do
        {
            cout<<endl<<endl<<"Voulez-vous convertir le nombre obtenue apres conversion en une autre base ?"<<endl
                <<"Veuillez repondre par (O/o) pour Oui ou par (N/n) pour Non "<<endl
                <<"Votre reponse : ";
            cin>>answer;

            while((answer != 'O' && answer != 'o') && (answer != 'N' && answer != 'n'))
            {
                cout<<"Veuillez entrer une reponse correcte s'il vous plait ! "<<endl
                    <<"Votre reponse : ";
                cin>>answer;
            }

            if(answer == 'O' || answer =='o')
            {
                baseOrigine=baseConversion;
                cout<<endl<<"Veuillez entrer la nouvelle base de conversion : ";
                baseConversion=cinBase();

                valeur=valeurRetour;
                valeurRetour=conversion(valeur, baseOrigine, baseConversion);
                sansZerosDebut(valeur);
                sansZerosDebut(valeurRetour);
                cout<<endl<<"Conversion"<<endl;
                cout<<"Base "<<baseOrigine<<" : "<<valeur<<" <=> "<<"Base "<<baseConversion<<" : "<<valeurRetour<<endl;
            }

        }
        while(answer == 'O' || answer == 'o');

        cout<<endl<<"Voulez-vous convertir un autre nombre ? "<<endl
            <<"Veuillez repondre par (O/o) pour Oui ou par (N/n) pour Non "<<endl
            <<"Votre reponse : ";
        cin>>answer;

        while((answer != 'O' && answer != 'o') && (answer != 'N' && answer != 'n'))
        {
            cout<<"Veuillez entrer une reponse correcte s'il vous plait ! "<<endl
                <<"Votre reponse : ";
            cin>>answer;
        }

    }
    while(answer == 'O' || answer == 'o');

    return 0;
}
