#include <iostream>
#include "fonctions.h"
#include <string>
#include <cmath>

using namespace std;

/// fonction appartientABaseElementValeur
bool appartientABaseElementValeur(char element, unsigned int base)
{
    bool appartientABase(false);
    string tabElementsBase(" ");  ///chaine de caract�res qui contiendra tous les �l�ments de chaque base
    unsigned int i(0), tailleTab(0);  /// taille de tabElementsBase

    switch(base)
    {
    case 2 :
    {
        tabElementsBase="01";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(element == tabElementsBase[i])
            {
                appartientABase=true;  /// appartient � base prend true si l'�l�ment entr� en param�tre appartient � la base
                i=tailleTab;
            }
        }

    }
    break;
    case 8 :
    {
        tabElementsBase="01234567";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(element == tabElementsBase[i])
            {
                appartientABase=true;
                i=tailleTab;
            }
        }
    }
    break;
    case 10 :
    {
        tabElementsBase="0123456789";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(element == tabElementsBase[i])
            {
                appartientABase=true;
                i=tailleTab;
            }
        }
    }
    break;
    case 16 :
    {
        tabElementsBase="0123456789ABCDEF";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(element == tabElementsBase[i])
            {
                appartientABase=true;
                i=tailleTab;
            }
        }
    }
    break;
    }
    return appartientABase;
}

/// fonction appartiennentABaseElementsValeur
bool appartiennentABaseElementsValeur(string& valeur, unsigned int base)
{
    bool appartiennentABase(false);
    unsigned int i(0), compteur(0), tailleValeur(valeur.size());

    for(i=0; i<tailleValeur; i++)
    {
        if(appartientABaseElementValeur(valeur[i], base) == true)
            compteur++;  /// le compteur est incr�ment� si un �l�ment de la valeur envoy� en param�tre appatient � la base d'�tude
    }
    if(compteur == tailleValeur)
        appartiennentABase=true;  ///appartientABase prend true si compteur est �gale � tailleValeur c'est � dire si tous les �l�ments de valeur appartiennent � la base d'�tude

    return appartiennentABase;
}

/// fonction cinBase
unsigned int cinBase( )
{
    string baseString(" ");

    cin>>baseString;

    while(appartiennentABaseElementsValeur(baseString, 10) == false)   /// on v�rifie si tous les caract�res de baseString sont des "chiffres"
    {
        cout<<"Veuillez entrer un nombre entier s'il vous plait ! "<<endl
        <<"Votre reponse : ";
        cin>>baseString;
    }

    unsigned int base(stoi(baseString));   /// conversion du contenu de baseString en entier avec la fonction stoi
    unsigned int tabBase[4]={2, 8, 10, 16};
    bool estDansTabBase(false);

    for(unsigned int i (0); i<4; i++)
    {
       if(base == tabBase[i])
       {
            estDansTabBase=true;   /// arr�t pr�coce de la boucle d�s que la base est une base d'�tude
            break;
       }
    }

    if(estDansTabBase==false)
    {
        cout<<"Veuillez entrer une base d'etude s'il vous plait !"<<endl
        <<"Votre reponse : ";
        base=cinBase();
    }

    return base;
}

/// fonction tabNull
void tabNull(unsigned int* tab, unsigned int tailleTab)
{
    for(unsigned int i(0); i<tailleTab; i++)
    {
        tab[i]=0;
    }
}

/// fonction nombreEquivalantElement
unsigned int nombreEquivalantElement(char element, unsigned int base)
{
    string tabElementsBase(" ");  ///chaine de caract�res qui contiendra tous les �l�ments de chaque base
    unsigned int i(0), tailleTab(0), nombre(0);

    switch(base)
    {
    case 2 :
    {
        tabElementsBase="01";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(element == tabElementsBase[i])   /// on v�rifie si l'�l�ment est idendique � un �l�ment de tabElementsBase
            {
                nombre=i;  /// si l'�l�ment est identique � un �l�ment de la base, alors sa valeur(nombre entier) est ins�r�e dans nombre
                /// l'insertion est fait avec i car les �l�ments de la base coincide avec i
                ///tabElementsBase[0] == 0, tabElementsBase[1] == 1;
                /// ce qui est de m�me pour les autres bases
                i=tailleTab;  /// arr�t forc� de la boucle
            }
        }

    }
    break;
    case 8 :
    {
        tabElementsBase="01234567";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(element == tabElementsBase[i])
            {
                nombre=i;
                i=tailleTab;
            }
        }
    }
    break;
    case 10 :
    {
        tabElementsBase="0123456789";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(element == tabElementsBase[i])
            {
                nombre=i;
                i=tailleTab;
            }
        }
    }
    break;
    case 16 :
    {
        tabElementsBase="0123456789ABCDEF";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(element == tabElementsBase[i])
            {
                nombre=i;
                i=tailleTab;
            }
        }
    }
    break;
    }

    return nombre;
}

/// fonction valeurElementsDansTab
void valeurElementsDansTab(string& valeur, unsigned int* tab, unsigned int base)
{
    unsigned int i(0), tailleValeur(valeur.size());

    for(i=0; i<tailleValeur; i++)
    {
        tab[i]=nombreEquivalantElement(valeur[i], base);  /// la valeur (nombre entier) de chaque �l�ment est ins�r� dans tab suivant l'ordre qu'il occupait dans valeur
        /// Exemple :  valeur >> "10804" <=> tab >> 10804
    }
}


///fonction elementEquivalantNombre
char elementEquivalantNombre(unsigned int nombre, unsigned int base)
{
    char element(' ');
    string tabElementsBase(" ");
    unsigned int i(0), tailleTab(0);

    switch(base)
    {
    case 2 :
    {
        unsigned int tabNombreBase[2]= {0, 1};
        tabElementsBase="01";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(nombre == tabNombreBase[i])  /// on v�rifie si le nombre envoy� en param�tre se trouve dans tabNombreBase
            {
                element=tabElementsBase[i];  /// si oui, le caract�re correspondant est stock� dans element (1 ==> '1')
                i=tailleTab;  /// arr�t forc� de la boucle si le caract�re correspondant est identifi�
            }
        }

    }
    break;
    case 8 :
    {
        unsigned int tabNombreBase[8]= {0, 1, 2, 3, 4, 5, 6, 7};
        tabElementsBase="01234567";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(nombre == tabNombreBase[i])
            {
                element=tabElementsBase[i];
                i=tailleTab;
            }
        }
    }
    break;
    case 10 :
    {
        unsigned int tabNombreBase[10]= {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        tabElementsBase="0123456789";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(nombre == tabNombreBase[i])
            {
                element=tabElementsBase[i];
                i=tailleTab;
            }
        }
    }
    break;
    case 16 :
    {
        unsigned int tabNombreBase[16]= {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        tabElementsBase="0123456789ABCDEF";
        tailleTab=tabElementsBase.size();

        for(i=0; i<tailleTab; i++)
        {
            if(nombre == tabNombreBase[i])
            {
                element=tabElementsBase[i];
                i=tailleTab;
            }
        }
    }
    break;
    }
    return element ;
}

/// fonction charactereNombreDansValeurRetour
void charactereNombreDansValeurRetour(unsigned int* tab, unsigned int tailleTab, string& valeurRetour, unsigned int base)
{
    unsigned int i(0);

    for(i=0; i<tailleTab; i++)
    {
        valeurRetour.push_back(elementEquivalantNombre(tab[i], base));
    }
}

/// fonction nombreRestesDivisionEuclidienne
unsigned int nombreRestesDivisionEuclidienne(unsigned int nombre, unsigned int base)
{
    int quotient(1), i(0);

    while(quotient !=0) /// repeter la division euclidienne tant que le quotient est diff�rent de 0
    {
        quotient=nombre/base;
        nombre=quotient;
        i++;  /// i est incr�ment� pour savoir le nombre de reste sont issus de la division eucludienne
    }
    return i;
}

/// fonction divisionEuclidienne
void divisionEuclidienne(unsigned int nombre, unsigned int base, unsigned int* tab, unsigned int tailleTab)
{
    int quotient(1), resteDivision(0), i(0);

    while(quotient !=0)  /// repeter la division euclidienne tant que le quotient est diff�rent de 0
    {
        quotient=nombre/base;
        resteDivision=nombre%base;
        tab[tailleTab-(i+1)]=resteDivision; /// les restes sont stock�s dans tab
        /// sachant que les restes snt ins�r�s de la derni�re case du tableau � la premi�re case afin de respecter les conventions/r�gles de la division euclidienne
        nombre=quotient; ///le quotient devient le nombre � diviser
        i++;
    }
}

/// fonction nombreFromBase
unsigned int nombreFromBase( unsigned int* tab, unsigned int tailleTab, unsigned int base)
{
    unsigned int i(0), nbre(0);

    for(i=0; i<tailleTab; i++)
    {
        nbre += tab[i] * pow(base, tailleTab-(i+1));  /// calcul du nombre correspondant suivant la r�gle de l'�levation � la puissance
    }
    return nbre;
}

/// fonction  plusDeZerosALaFinDeValeur
bool  plusDeZerosALaFinDeValeur(std::string& valeur, unsigned int nombreZeros)
{
    bool verification(false);
    unsigned int compteur(0);

    for(unsigned int i(0); i<nombreZeros; i++)
    {
        if(valeur[i] == '0')
            compteur++;
    }
    if(compteur == nombreZeros)
        verification=true;

    return verification;
}

/// fonction zerosRamenesAuDebut
void zerosRamenesAuDebut(std::string& valeur, unsigned int nombreZeros)
{
    unsigned int tailleValeur(valeur.size());
    char last(' ');

    while( plusDeZerosALaFinDeValeur(valeur, nombreZeros) == false)
    {
        last=valeur[tailleValeur-1];
        for( unsigned int i(0); i<(tailleValeur-1); i++)
        {
            valeur[tailleValeur-(i+1)]=valeur[tailleValeur-(i+2)];
        }
        valeur[0]=last;
    }
}

/// fonction sansZerosDebut
void sansZerosDebut(std::string& valeur)
{
    while(valeur[0] == '0')
    {
        valeur.erase(0, 1);
    }
}

/// fonction lettreCapitale
void lettreCapitale(std::string& valeur)
{
    int tailleValeur(valeur.size());

    for(int i(0); i<tailleValeur; i++)
    {
        valeur[i]=toupper(valeur[i]);
    }
}

