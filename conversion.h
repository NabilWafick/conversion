#ifndef CONVERSION_H_INCLUDED
#define CONVERSION_H_INCLUDED


/** fonction conversionBinaireOctal
*
* role : se charge de de convertir un nombre binaire(base 2) en octal(base 8)
* parametre : valeur >> valeur de la base � convertir en octal (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 2)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionBinaireOctal(std::string& valeur);

/** fonction conversionBinaireDecimal
*
* role : se charge de de convertir un nombre binaire(base 2) en d�cimal(base 10)
* parametre : valeur >> valeur de la base � convertir en d�cimal (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 2)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionBinaireDecimal(std::string& valeur);

/** fonction conversionBinaireHexadecimal
*
* role : se charge de de convertir un nombre binaire(base 2)  en base hexad�cimale(base 16)
* parametre : valeur >> valeur de la base � convertir en hexad�cimal (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 2)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionBinaireHexadecimal(std::string& valeur);

/** fonction conversionOctalBinaire
*
* role : se charge de de convertir un nombre octal(base 8)  en binaire(base 2)
* parametre : valeur >> valeur de la base � convertir en binaire (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 8)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionOctalBinaire(std::string& valeur);

/** fonction conversionOctalDecimal
*
* role : se charge de de convertir un nombre octal(base 8) en d�cimal(base 10)
* parametre : valeur >> valeur de la base � convertir en d�cimal (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 8)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionOctalDecimal(std::string& valeur);

/** fonction conversionOctalHexadecimal
*
* role : se charge de de convertir un nombre octal(base 8)  en hexad�cimal(base 16)
* parametre : valeur >> valeur de la base � convertir en hexad�cimal (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 8)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversioOctalHexadecimal(std::string& valeur);

/** fonction conversionDecimalBinaire
*
* role : se charge de de convertir un nombre d�cimal(base 10)  en binaire(base 2)
* parametre : valeur >> valeur de la base � convertir en binaire (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 10)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionDecimalBinaire(std::string& valeur);

/** fonction conversionDecimalOctal
*
* role : se charge de de convertir un nombre d�cimal(base 10)  en octal(base 8)
* parametre : valeur >> valeur de la base � convertir en binaire (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 10)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionDecimalOctal(std::string& valeur);

/** fonction conversionDecimalHexadecimal
*
* role : se charge de de convertir un nombre d�cimal(base 10)  en hexad�cimal(base 16)
* parametre : valeur >> valeur de la base � convertir en hexad�cimal (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 10)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionDecimalHexadecimal(std::string& valeur);

/** fonction conversionHexadecimalBinaire
*
* role : se charge  de convertir un nombre hexadecimal(base 16)  en binaire(base 2)
* parametre : valeur >> valeur de la base � convertir en binaire (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 16)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionHexadecimalBinaire(std::string& valeur);

/** fonction conversionHexadecimalOctal
*
* role : se charge de de convertir un nombre hexadecimal(base 16)  en octal(base 8)
* parametre : valeur >> valeur de la base � convertir en binaire (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 16)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionHexadecimalOctal(std::string& valeur);

/** fonction conversionHexadecimalDecimal
*
* role : se charge de de convertir un nombre hexadecimal(base 16) en d�cimal(base 10)
* parametre : valeur >> valeur de la base � convertir en d�cimal (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 16)
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionHexadecimalDecimal(std::string& valeur);

/** fonction conversion
*
* role : se charge de de convertir une valeur d'une base en une autre
* parametre : valeur >> valeur � convertir (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 16)
                        baseOrigine >> base dans laquelle se trouve la valeur
                        baseConversion >> base dans laquelle sera convertie la+ valeur
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversion(std::string& valeur, unsigned int baseOrigine,  unsigned int baseConversion);


#endif // CONVERSION_H_INCLUDED
