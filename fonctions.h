#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED

/** fonction cinBase
*
* role : se charge de r�ecup�rer la valeur correcte de la base
* parametre :
* retourne : la valeur de la base(nombre entier)
*
*/
unsigned int cinBase( );


/** fonction lettreCapitale
*
* role : se charge de metttre en capital les lettres de la cha�ne de caract�res pass�e en param�tre (d�di� sp�cialement � la base 16)
* parametre : valeur >> valeur dont on veut mettre en capital
* retourne : rien
*
*/
void lettreCapitale(std::string& valeur);

/** fonction valeurCorrecte
*
* role : se charge d'amener l'utilisateur � enter une valeur correcte
* parametre : expression >> expression de la valeur � entrer (un nombre, une phrase ...)
* retourne : la valeur correcte
*
*/
unsigned int valeurCorrecte(std::string expression);

/** fontion appartientABaseElementValeur
*
* role : se charge de verifier si un nombre est �l�ment de la base( base 2, 8, 10, 16)
* parametre : element >> �l�ment dont on veut v�rifier l'appartenance � la base de v�rification
                        base >> base de v�rification
* retourne : un bool (true si l'�l�ments de la valeur appartient � la base de v�rification , false sinon)
*
*/
bool appartientABaseElementValeur(char element, unsigned int base);

/** fonction appartiennentABaseElementsValeur
*
* role : se charge de verifier si tous les �l�ments composant la valeur entr�e en param�tre appartiennent � la base de v�rification(base 2, 8, 10, 16)
* parametre : valeur >> valeur dont on veut v�rifier si les �l�ments appartiennent � la base
                        base >> base de v�rification
* retourne : un bool (true si tous les �l�ments de la valeur appartiennent � la base de v�rification , false sinon)
*
*/
bool appartiennentABaseElementsValeur(std::string& valeur, unsigned int base);

/** foncti0n tabNull
*
* role : se charge de remplacer tout le contnu du tableau par z�ro
* parametre : tab >> tableau dont on veut mettre tout le contenu � z�ro
                       tailleTab >> taille du tableau
* retourne : rien
*
*/
void tabNull(unsigned int* tab, unsigned int tailleTab);

/** fonction nombreEquivalantElement
*
* role : se charge d'identifier le nombre correspondant � un �l�ment dans la base d'�tude( base 2, 8, 10, 16)
            Exemple : ( '1' == 1, '2' == 2, '3' == 3, 'A' == 10, 'F' == 15 )
* parametre : element >> �l�ment dont on veut chercher le nombre correspondant
                        base >> base d'�tude
* retourne : le nombre correspondant � l'�l�ment
*
*/
unsigned int nombreEquivalantElement(char element, unsigned int base);

/** fonction valeurElementsDansTab
*
* role : se charge d'ins�rer dans le tableau entrer en param�tre le nombre �quivalent � chaque �l�ment de valeur
            Exemple : ( '1' == 1, '2' == 2, '3' == 3, 'A' == 10, 'F' == 15 )
* parametre : valeur >> chaine contenant les �l�ments dont on veut chercher le nombre correspondant
                        tab >> tableau contenant devant contenir les nombres correspondants aux �l�ments de valeur
                        ( a la m�me taille que valeur)
*
* retourne : rien
*/
void valeurElementsDansTab(std::string& valeur, unsigned int* tab, unsigned int base);

/** fonction elementEquivalantNombre
*
* role : se charge d'identifier le caract�re correspondant � un nombre dans la base d'�tude( base 2, 8, 10, 16)
            Exemple : ( 1 == '1', 2 == '2', 3 == '3', 10 == 'A', 15 == 'F' )
* parametre : nombre >> nombre dont on veut chercher le charact�re correspondant
                        base >> base d'�tude
* retourne : le charact�re correspondant au nombre
*
*/
char elementEquivalantNombre(unsigned int nombre, unsigned int base);

/** fonction charactereNombreDansValeurRetour
*
* role : se charge d'ins�rer dans valeurRetour(cha�ne de caract�re � retourner) entrer en param�tre le nombre �quivalent
            � chaque nombre du tableau entrer en param�tre
            Exemple : ( 1 == '1', 2 == '2', 3 == '3', 10 == 'A', 15 == 'F' )
* parametre : tab >> tableau contenant les nombres dont on veut chercher les charact�res correspondants
                        tailleTab >> taille du tableau pass� en param�tre
                        valeurRetour >> chaine contenant les characteres correspondants aux nombres de tab
* retourne : rien
*
*/
void charactereNombreDansValeurRetour(unsigned int* tab, unsigned int tailleTab, std::string& valeurRetour, unsigned int base);

/** fonction nombreRestesDivisionEuclidienne
*
* role : se charge de determiner le nombre de reste de la division euclidienne d'un nombre par une base
            afin de derterminer la taille normale du tableau � allouer pouvant contenir les restes de la division euclidienne
* parametre : nombre >> nombre dont veut faire la division euclidienne dans la base
                        base >> base dans laquelle se fera la division
* retourne : le nombre de restes de la division euclidienne
*
*/
unsigned int nombreRestesDivisionEuclidienne(unsigned int nombre, unsigned int base);

/** fonction divisionEuclidienne
*
* role : se charge de determiner les restes issus de la division euclidienne du nombre entr� en param�tre par une base
            lesquels restes seront stock�s dans le tableau entr� en param�tre
* parametre : nombre >> nombre dont veut faire la division euclidienne
                        base >> base par laquelle se fera la division
                        tab >> tableau devant contenir les restes de la division euclidienne
                        tailleTab >> taille du tableau entr� en param�tre
* retourne : rien
*
*/
void divisionEuclidienne(unsigned int nombre, unsigned int base, unsigned int* tab, unsigned int tailleTab);


/** fonction nombreFromBase
*
* role : se charge de d�terminer le nombre d�cimal �quivalente � une valeur dans une autre base(2, 8 ou 16)
* parametre : tab >> tableau d'entier de la base � convertir
                        tailleTab >> taille du tableau d'entier
                        base : base de laquelle sont les �l�ments du tableau
* retourne : le nombre �quivalent
*
*/
unsigned int nombreFromBase( unsigned int* tab, unsigned int tailleTab, unsigned int base);

/** fonction conversionDecimalBase
*
* role : se charge de de convertir un nombre decimal(base 10) en une autre base1(base 2 ou 8)
* parametre :  valeur >> valeur de la base � convertir en decimal (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 10)
                        base >> base de conversion
* retourne : valeur obtenue apr�s conversion en la base de conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionDecimalBase(std::string& valeur, unsigned int base);

/** fonction conversionBaseDecimal
*
* role : se charge de de convertir un nombre d'une base(base 2, 8 ou 16) en base decimale(base 10)
* parametre : valeur >> valeur de la base � convertir en decimal (utilisation d'un string en raison de l'incapacit� � stocker la valeur enti�re
                                        directement dans un tableau d'entier  et pour v�rifier si les �l�ments de la valeur entr�e appartiennent � la base 2, 8 ou 16)
                        base >> base de conversion
* retourne : la valeur issue de la conversion(sous forme de string en vue d'une utilisation ult�rieure)
*
*/
std::string conversionBaseDecimal(std::string& valeur, unsigned int base);

/** fonction sansZerosDebut
*
* role : se charge de supprimer tout z�ros se trouvant au debut de la valeur pass� en param�tre
* parametre : valeur >> valeur dont on veut d�nuder du pr�fixe z�ros
* retourne : rien
*
*/
void sansZerosDebut(std::string& valeur);

/** fonction  plusDeZerosALaFinDeValeur
*
* role : se charge de v�rifier si tous les z�ros ajout�s � la fin de la valeur sont tous ramen�s au d�but
* parametre : mot >> valeur � la fin duquel sont ajout�es des z�ros
                      nombreZeros >> nombre de zeros ajout�s � la fin de la valeur
* retourne : bool(true si tous les z�ros sont � la fin, false sinon)
*
*/
bool  plusDeZerosALaFinDeValeur(std::string& valeur, unsigned int nombreZeros);

/** foncction zerosRamenesAuDebut
*
* role : se charge de v�rifier si tous les z�ros ajout�s � la fin de la valeur sont tous ramen�s au d�but
* parametre : mot >> valeur � la fin duquel sont ajout�es des z�ros
                      nombreZeros >> nombre de zeros ajout�s � la fin de la valeur
* retourne : rien
*
*/
void  zerosRamenesAuDebut(std::string& valeur, unsigned int nombreZeros);

/** fonction lettreCapitale
*
* role : se charge de metttre en capital les lettres de la cha�ne de caract�res pass�e en param�tre (d�di� sp�cialement � la base 16)
* parametre : valeur >> valeur dont on veut mettre en capital
* retourne : rien
*
*/
void lettreCapitale(std::string& valeur);

#endif // FONCTIONS_H_INCLUDED
